import { Component, OnInit } from '@angular/core';
import { destinoviaje } from "../models/destino-viaje.models"
import { DestinoViajeComponent } from '../destino-viaje/destino-viaje.component';

@Component({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css']
})

export class ListadestinosComponent implements OnInit {

  destinos: destinoviaje[];
  constructor() {
    this.destinos=[];

  }

  ngOnInit() {

  }

  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new destinoviaje(nombre,url));
    return false;

  }

  elegido(destino:destinoviaje) {
    this.destinos.forEach(function (x) {x.setSelected(false); });
    destino.setSelected(true);

  }

}
