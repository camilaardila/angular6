import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import {destinoviaje} from './../models/destino-viaje.models';



@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino:destinoviaje;
  @Input('idx') position: number;
  @HostBinding('attr.Class')cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<destinoviaje>;

  constructor() {
    this.clicked= new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

}
